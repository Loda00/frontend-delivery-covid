import React from 'react'
import { Switch, Route } from 'react-router-dom'

import Products from './Products'
import Create from './Create'
import NotFound from 'routes/404'

const Web = () => {
  return (
    <Switch>
      <Route exact path="/product" component={Products} />
      <Route path="/product/create" component={Create} />
      <Route component={NotFound} />
    </Switch>
  )
}

export default Web
